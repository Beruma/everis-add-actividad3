package dam2.add.p3;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

public class Jugar {

    static ArrayList<Pregunta> lista_preguntas = new ArrayList<>();
    static ArrayList<Jugador> lista_jugadores = new ArrayList<>();

    //Este método lee las preguntas alojadas en el xml antes de empezar el juego y las almacena en un arraylist//
    public static void leerPreguntas(){

        SAXBuilder saxBuilder = new SAXBuilder();
        File xmlFile = new File("./ficheros/preguntas.xml");

        try {
            Document document = saxBuilder.build(xmlFile);

            Element rootNode = document.getRootElement();
            String nombreNodo = rootNode.getName();

            List<Element> lista_preguntas = rootNode.getChildren("pregunta");
            for (int i = 0; i <lista_preguntas.size() ; i++) {
                Pregunta p = new Pregunta();
                Element pregunta = (Element) lista_preguntas.get(i);
                String texto = pregunta.getChildText("texto");
                p.setPregunta(texto);
                String respuesta1 = pregunta.getChildText("respuesta1");
                p.setRespuesta1(respuesta1);
                String respuesta2 = pregunta.getChildText("respuesta2");
                p.setRespuesta2(respuesta2);
                String respuesta3 = pregunta.getChildText("respuesta3");
                p.setRespuesta3(respuesta3);
                String correcta = pregunta.getChildText("correcta");
                p.setCorrecta(correcta);
                Jugar.lista_preguntas.add(p);
            }

        } catch (JDOMException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //Este método es el que le va mostrando las preguntas al usuario y guardando las respeustas, además va generando el pdf//
    public static void play(){
        Pregunta pg2 = new Pregunta();
        Scanner key = new Scanner(System.in);
        int indice = 0;
        String respuesta = "";
        int puntos = 0;
        System.out.println("Elige la respuesta correcta: ");
        PdfWriter pdfWriter = null;
        com.itextpdf.text.Document document = new com.itextpdf.text.Document(PageSize.A4, 20 , 20 , 70, 50);

        try {
            pdfWriter = PdfWriter.getInstance(document, new FileOutputStream("./ficheros/partida.pdf", true));
            document.open();
            Paragraph paragraph = new Paragraph();
            do {

                pg2 = lista_preguntas.get(indice);
                String pregunta = pg2.getPregunta().toString();
                String respuesta1 = pg2.getRespuesta1().toString();
                String respuesta2 = pg2.getRespuesta2().toString();
                String respuesta3 = pg2.getRespuesta3().toString();
                String correcta = pg2.getCorrecta().toString();
                System.out.println(pregunta);
                System.out.println(respuesta1);
                System.out.println(respuesta2);
                System.out.println(respuesta3);
                respuesta = key.nextLine();
                System.out.println(correcta);

                if ((pg2.getCorrecta().equalsIgnoreCase("1")) && (respuesta.equalsIgnoreCase(pg2.getRespuesta1()))) {
                    puntos++;
                } else if ((pg2.getCorrecta().equalsIgnoreCase("2")) && (respuesta.equalsIgnoreCase(pg2.getRespuesta2()))) {
                    puntos++;
                } else if ((pg2.getCorrecta().equalsIgnoreCase("3")) && (respuesta.equalsIgnoreCase(pg2.getRespuesta3()))) {
                    puntos++;
                }
                paragraph.add("Pregunta: " + pregunta + "\n");
                paragraph.add("Respuesta 1: " + respuesta1 + "\n");
                paragraph.add("Respuesta 2: " + respuesta2  + "\n");
                paragraph.add("Respuesta 3: " + respuesta3 + "\n");
                paragraph.add("Tu respuesta fue: " + respuesta + "\n");
                paragraph.add("Y la respuesta correcta es la respuesta: " + correcta + "\n");


                indice++;
            } while ((respuesta != null) && (indice <= lista_preguntas.size() - 1));

            System.out.println("Tu puntuación final es: " + puntos);
            paragraph.add("Estos son tus puntos: " + puntos);
            document.add(paragraph);
            document.close();
            pdfWriter.close();
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        System.out.println("¿Quieres ver el informe de la partida?(si/no)");
        respuesta = key.nextLine();
        if (respuesta.equalsIgnoreCase("si")) {
            try {
                File path = new File("./ficheros/partida.pdf");
                Desktop.getDesktop().open(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        inscribirRecord(puntos);
    }
    //Este método te deja añadir una pregunta//
    public static void añadirPregunta(){
        Pregunta pg3 = new Pregunta();
        Scanner key = new Scanner(System.in);
        String pregunta;
        String respuesta1;
        String respuesta2;
        String respuesta3;
        String correcta;
        System.out.println("Escriba la pregunta");
        pregunta = key.nextLine();
        pg3.setPregunta(pregunta);
        System.out.println("Escribe tus respuestas e indica al final cual es la correcta (1, 2, 3)");
        System.out.println("escribe la preimera respuesta");
        respuesta1 = key.nextLine();
        pg3.setRespuesta1(respuesta1);

        System.out.println("escribe la segunda respuesta");
        respuesta2 = key.nextLine();
        pg3.setRespuesta2(respuesta2);

        System.out.println("escribe la tercera respuesta");
        respuesta3 = key.nextLine();
        pg3.setRespuesta3(respuesta3);

        boolean check = false;
        do {
            System.out.println("Dime cual es la correcta (1, 2, 3)");
            correcta = key.nextLine();
            if ((correcta.equalsIgnoreCase("1"))||(correcta.equalsIgnoreCase("2"))||(correcta.equalsIgnoreCase("3"))){
                pg3.setCorrecta(correcta);
                check = true;
            }else{
                check = false;
            }
        }while(!check);
        lista_preguntas.add(pg3);
    }

    //Este método coge las preguntas desde el excell//
    public static void importarExcell(){
        File file = new File("./ficheros/preguntas.xls");
        try{
            Workbook w = Workbook.getWorkbook(file);

            Sheet sheet = w.getSheet(0);
            for (int f = 0; f < sheet.getRows(); f++) {
                    Pregunta p = new Pregunta();
                    String pregunta = sheet.getCell(0,f).getContents();
                    p.setPregunta(pregunta);
                    String respuesta1 = sheet.getCell(1,f).getContents();
                    p.setRespuesta1(respuesta1);
                    String respuesta2 = sheet.getCell(2,f).getContents();
                    p.setRespuesta2(respuesta2);
                    String respuesta3 = sheet.getCell(3,f).getContents();
                    p.setRespuesta3(respuesta3);
                    String correcta = sheet.getCell(4,f).getContents();
                    p.setCorrecta(correcta);
                    lista_preguntas.add(p);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BiffException e) {
            e.printStackTrace();
        }
    }

    //Este método sobreescribe el xml añadiendo las preguntas nuevas a las que ya estaban//
    public static void escribirXml(){
        String docNuevoStr = "";

        try{
            Document document = new Document();

            Element nodoRaiz = new Element("juego");
            document.addContent(nodoRaiz);

            for (Pregunta p: lista_preguntas) {

                Element nodoPregunta = new Element("pregunta");
                nodoRaiz.addContent(nodoPregunta);

                Element nodoTexto = new Element("texto");
                nodoTexto.setText(p.getPregunta());
                nodoPregunta.addContent(nodoTexto);

                Element nodoRespuesta1 = new Element("respuesta1");
                nodoRespuesta1.setText(p.getRespuesta1());
                nodoPregunta.addContent(nodoRespuesta1);

                Element nodoRespuesta2 = new Element("respuesta2");
                nodoRespuesta2.setText(p.getRespuesta2());
                nodoPregunta.addContent(nodoRespuesta2);

                Element nodoRespuesta3 = new Element("respuesta3");
                nodoRespuesta3.setText(p.getRespuesta3());
                nodoPregunta.addContent(nodoRespuesta3);

                Element nodoCorrecta = new Element("correcta");
                nodoCorrecta.setText(p.getCorrecta());
                nodoPregunta.addContent(nodoCorrecta);
            }

            Format format = Format.getPrettyFormat();
            XMLOutputter xmlOutputter = new XMLOutputter(format);
            docNuevoStr = xmlOutputter.outputString(document);

        }catch (Exception e){
            System.out.println("error al crear el xml");
        }
        FileWriter fileWriter = null;
        try{
            fileWriter = new FileWriter("./ficheros/preguntas.xml");
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.println(docNuevoStr);
            fileWriter.close();
        }catch (IOException e){
            System.out.println("Error al escribir el xml");
        }
    }

    //Este método agrega los records de los jugadores a un arraylist//
    public static void inscribirRecord(int puntos){

        Scanner key = new Scanner(System.in);
        System.out.println("Introduzca su nombre");
        String nombre = key.nextLine();
        Jugador j = new Jugador();
        if (comprobarJugador(nombre)){
            if (comprobarPuntuacion(nombre, puntos)) {
                for (int i = 0; i <lista_jugadores.size() ; i++) {
                    if (nombre.equalsIgnoreCase(lista_jugadores.get(i).getNombre()))
                    lista_jugadores.get(i).setPuntuacion(puntos);
                }
            }
        }else {
            j.setNombre(nombre);
            j.setPuntuacion(puntos);
            lista_jugadores.add(j);
        }
    }

    //este método comprueba si el jugador ha jugado anteriormente//
    public static boolean comprobarJugador(String nombre) {
        for (Jugador j : lista_jugadores) {
            if (nombre.equalsIgnoreCase(j.getNombre())) {
                return true;
            }

        }
        return false;
    }
    //este comprueba la puntuacion obtenida de un jugador que haya jugado anteriormente y las compara//
    public static boolean comprobarPuntuacion(String nombre, int puntos){
        for (Jugador j : lista_jugadores) {
            if (nombre.equalsIgnoreCase(j.getNombre())) {
                if (puntos > j.getPuntuacion()){

                    return true;
                }
            }

        }
        return false;
    }
    //Este método permite visualizar el array con las puntuaciones ordenados de mayor a menor//
    public static void visualizarRecords(){
        Collections.sort(lista_jugadores, new Comparator<Jugador>() {
            @Override
            public int compare(Jugador o1, Jugador o2) {
                return new Integer(o2.getPuntuacion()).compareTo(new Integer(o1.getPuntuacion()));
            }
        });
        System.out.println(lista_jugadores);
        grabarRecords();
    }
    //este método se inicia al principio para añadir al array las puntuaciones guardadas en partidas anteriores//
    public static void leerRecords() {
        File file = new File("./ficheros/records.txt");
        if (file.exists()) {
            ObjectInputStream objectInputStream = null;
            try {
                objectInputStream = new ObjectInputStream(new FileInputStream(file));

                lista_jugadores = (ArrayList<Jugador>) objectInputStream.readObject();
                objectInputStream.close();
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
    //Este método graba las puntuaciones en un txt//
    public static void grabarRecords() {
    try{
        FileOutputStream fileOutputStream = new FileOutputStream("./ficheros/records.txt");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(lista_jugadores);
        objectOutputStream.close();
    } catch (FileNotFoundException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
    }
    //este metodo muestra las intrucciones del juego//
    public static void mostrarInstrucciones(){
        System.out.println("Instrucciones de juego:");
        System.out.println("1. Es un trivial, donde puedes encontrar preguntas variadas, todas con tres respuestas.\n" +
                "Pero solo UNA es la correcta.\n"+
                "2. El sistema de puntuación es sencillo, ganas un punto por respuesta acertada.\n"+
                "Gana quien más respuestas acierte, pero...la lista de preguntas se actualizará.\n"+
                "Por lo que hoy, puede que la mayor puntuación que se puede obtener son 10 puntos, pero mañana pueden ser 100.\n"+
                "3. Te deseo suerte ♥"
                );
    }
}