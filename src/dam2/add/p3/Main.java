package dam2.add.p3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner key = new Scanner(System.in);
        boolean salir = false;
        Jugar.leerPreguntas();
        Jugar.leerRecords();
        while (!salir) {
            System.out.println("Elige una opción: ");
            System.out.println("1. Jugar partida");
            System.out.println("2. Añadir preguntas");
            System.out.println("3. Importar preguntas");
            System.out.println("4. Ver records");
            System.out.println("5. Intrucciones");
            System.out.println("6. Salir ");

            int opcion = key.nextInt();

            switch (opcion) {
                case 1:
                    Jugar.play();
                    break;
                case 2:
                    Jugar.añadirPregunta();
                    break;
                case 3:
                    Jugar.importarExcell();
                    break;
                case 4:
                    Jugar.visualizarRecords();
                    break;
                case 5:
                   Jugar.mostrarInstrucciones();
                    break;
                case 6:
                    Jugar.escribirXml();
                    salir = true;
                    break;
                default:
                    System.out.println("Opción no válida por favor elija un número del 1 al 5");
            }

        }
    }
}
