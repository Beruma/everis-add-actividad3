package dam2.add.p3;

public class Pregunta {
    String pregunta;
    String respuesta1;
    String respuesta2;
    String respuesta3;
    String correcta;

    public String getCorrecta() {
        return correcta;
    }

    public void setCorrecta(String correcta) {
        this.correcta = correcta;
    }

    public String getRespuesta1() {
        return respuesta1;
    }

    public void setRespuesta1(String respuesta1) {
        this.respuesta1 = respuesta1;
    }

    public String getRespuesta2() {
        return respuesta2;
    }

    public void setRespuesta2(String respuesta2) {
        this.respuesta2 = respuesta2;
    }

    public String getRespuesta3() {
        return respuesta3;
    }

    public void setRespuesta3(String respuesta3) {
        this.respuesta3 = respuesta3;
    }



    public Pregunta() {

    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    @Override
    public String toString() {
        return "Pregunta: \n" +
                "pregunta: " + pregunta + '\'' +
                ", respuesta1: " + respuesta1 + '\'' +
                ", respuesta2: " + respuesta2 + '\'' +
                ", respuesta3: " + respuesta3 + '\'' +
                ", correcta: " + correcta + '\'';
    }
}
