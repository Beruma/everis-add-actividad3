package dam2.add.p3;

import java.io.Serializable;

public class Jugador implements Serializable {
    String nombre;
    int puntuacion;

    public Jugador() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    @Override
    public String toString() {
        return "Jugador: \n" +
                "nombre: " + nombre + '\'' +
                ", puntuacion: " + puntuacion +"\n";
    }
}
