1. Lo primero he creado la clase Main: donde le he puesto el menú. Lo cuál también me ha servido un poco de guía para
saber que métodos ir haciendo en la clase Jugar.
2. He creado la clase Pregunta (clase POJO) con sus atributos.
3. La clase Jugar donde he creado los métodos que se llaman en el Main y en la propia clase:
    -Primero he creado un método(leerPreguntas) para que me lea las preguntas del fichero preguntas.xml, y las agregue
    al ArrayList lista_preguntas. Este método lo llamaré al arrancar el programa.
    -Después he creado play, es el método que te muestras las preguntas(desde el ArrayList), las respuestas posibles,
    guarda la respuesta que da el usuario, y le muestra la respuesta correcta. Además de sumar los puntos si la
    respuesta es correcta.
    En este método tuve problemas porque lo quería separar de como hacer el pdf, pero me daba muchos fallos, por lo que
    al final decidí dejarlo junto. Así que en este método también se puede encontrar como agregar todo al pdf para hacer
    el informe.
    -En este método (añadirPregunta) el usuario podrá añadir una pregunta manualmente. Se comprobará que la respuesta
    correcta este contenida entre las 3 que el usuario te ha dado.
    -El siguiente método (importarExcell), lo que hace es leer el fichero preguntas.xls que el usuario puede manipular
    agregando directamente en el las preguntas. Cada pregunta ocupara una fila, y en cada fila dividida en los huecos
    estarán el texto de la pregunta, las respeustas y cual es correcta.
    -Al finalizar el programa encontramos el método (escribirXml), este método sobre escribe el fichero "preguntas.xml",
    con todas las preguntas que se encuentran en lista_preguntas, tanto las que ya estaban en el documento preguntas.xml,
    como las que se han añadido o importado.
4. He creado otra clase POJO llamada Jugador, con atributos nombre y puntos.
5. En la clase Jugar he implementado los métodos que haran uso de la clase Jugador:
    -Método incribirRecord(puntos), este método se le llamará en el método play(), al final. En el podemos ver como se
    le pide el nombre al usuario y se le pasan los puntos obtenidos en play(), para agregarlos al arrayList
    lista_jugadores. Con el comprobamos que en dicha lista no este repetido el mismo jugador, y si está, se comparará
    la puntuación obtenida con la puntuación anterior y se dejará la más alta. Todo esto lo hace con la ayuda de
    dos métodos, comprobarJugador() y comprobarPuntuacion(). Que también se hayan en la clase Jugar.
    -También podemos encontrar el método visualizarrecords(). Este método te muestra la lista_jugadores colocada con
    la puntuación de mayor a menor de los jugadores.
    -Al método leerRecords(), lo llamo también al principio del Main, para tener en el arrayList lista_jugadores y así
    poder manipularla con más facilidad.
    -Encontramos grabarRecords() método al que llamo en visualizarRecords() para que cuando se guarden en
    el fichero records.txt se guarden ordenados con la puntuación de mayor a menor.
6. Y por último un método, mostrarInstrucciones(), que contiene una salida por pantalla donde se muestran las
    intrucciones del juego y como funciona la puntuación.
